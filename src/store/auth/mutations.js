import http from '@/plugins/axios'
import TokenStorage from '@/core/auth/token-storage'
import { retrieveUserId, retrieveCalculationIn, isAdmin } from '@/utils'
import types from './mutation-types'

export default {
  [types.SET_TOKEN] (state, token) {
    state.user.token = token

    if (token) {
      TokenStorage.set(token)
      state.user.id = retrieveUserId(token)
      state.user.calculationIn = retrieveCalculationIn(token)
      state.user.isAdmin = isAdmin(token)
      http.defaults.headers.common.Authorization = `Bearer ${token}`
    } else {
      TokenStorage.remove()
      state.user.id = null
      state.user.calculationIn = null
      state.user.isAdmin = null
      delete http.defaults.headers.common.Authorization
    }
  },

  [types.SET_LOADING] (state, loading) {
    state.loading = loading
  },

  [types.SET_USER] (state, user) {
    state.user.data = user
  }
}
