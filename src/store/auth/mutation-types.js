export const SET_TOKEN = 'SET_TOKEN'
export const SET_LOADING = 'SET_LOADING'
export const SET_USER = 'SET_USER'

export default {
  SET_TOKEN,
  SET_LOADING,
  SET_USER
}
