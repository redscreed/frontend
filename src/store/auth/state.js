import TokenStorage from '@/core/auth/token-storage'
import { retrieveCalculationIn, isAdmin } from '@/utils'

const token = TokenStorage.get()

export default {
  user: {
    id: null,
    token,
    expiresIn: null,
    data: null,
    calculationIn: retrieveCalculationIn(token),
    isAdmin: isAdmin(token)
  },
  loading: false
}
