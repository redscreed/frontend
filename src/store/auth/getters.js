import TokenStorage from '@/core/auth/token-storage'

export const user = (state) => state.user.data

export const loading = (state) => state.loading

export const token = (state) => state.user.token

export const authenticated = (state) => () => TokenStorage.checkToken(state.user.token)

export const calculationIn = (state) => state.user.calculationIn

export const userId = (state) => state.user.id

export default {
  user,
  loading,
  token,
  authenticated,
  userId,
  calculationIn,
}
