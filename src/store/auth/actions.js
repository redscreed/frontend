import auth from '@/api/authApi'
import { SET_TOKEN, SET_LOADING } from './mutation-types'

export const login = async ({ commit }, credentials) => {
  try {
    commit(SET_LOADING, true)
    const response = await auth.login(credentials)

    commit(SET_TOKEN, response.token)
    commit(SET_LOADING, false)

    return response
  } catch (error) {
    commit(SET_LOADING, false)

    throw error
  }
}

export const register = async ({ commit }, account) => {
  try {
    commit(SET_LOADING, true)
    await auth.register(account)
    commit(SET_LOADING, false)
  } catch (error) {
    commit(SET_LOADING, false)

    throw error
  }
}

export const logout = ({ commit }, account) => {
  commit(SET_TOKEN, null)
}

export const confirmEmail = async ({ commit }, { id, expires, signature }) => {
  try {
    commit(SET_LOADING, true)
    await auth.confirmEmail(id, expires, signature)
    commit(SET_LOADING, false)
  } catch (error) {
    commit(SET_LOADING, false)

    throw error
  }
}

export default {
  login,
  register,
  logout
}
