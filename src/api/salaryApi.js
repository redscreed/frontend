import BaseApi from './BaseApi'

class SalaryApi extends BaseApi {

  async create(salary){
    try {
      const {data: response} = await this.http.post(`salary/create`, salary)
      return response
    } catch (error) {
      throw error
    }
  }

  async get(id){
    try {
      const {data: response} = await this.http.get(`salary/${id}/get`)
      return response
    } catch (error) {
      throw error
    }
  }

  async update(id, data){
    try {
      const {data: response} = await this.http.post(`salary/${id}/update`, data)
      return response
    } catch (error) {
      throw error
    }
  }

  async delete(id){
    try {
      const {data: response} = await this.http.get(`salary/${id}/delete`)
      return response
    } catch (error) {
      throw error
    }
  }

  async all(){
    try {
      const {data: response} = await this.http.get(`salary/all`)
      return response
    } catch (error) {
      throw error
    }
  }

  async allByUser(){
    try {
      const {data: response} = await this.http.get(`salary/allByUser`)
      return response
    } catch (error) {
      throw error
    }
  }

}

const salaryApi = new SalaryApi()

export default salaryApi

