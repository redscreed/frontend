import http from '../plugins/axios'

export const login = (credentials) => {
  return http.post('auth/login', credentials).then(
    (response) => {
      const { access_token: token, expires_in: expiresIn } = response.data
      return { token, expiresIn }
    }
  )
}

export const register = (user) => {
  return http.post('auth/register', user)
}

export const logout = () => {
  return http.post('auth/logout')
}

export const me = () => {
  return http.post('auth/me')
}

export default {
  login,
  register,
  logout,
  me
}
