import http from '@/plugins/axios'
import axios from 'axios'

const CancelToken = axios.CancelToken

export default class BaseApi {
  constructor () {
    this.http = http
  }

  token () {
    return CancelToken.source()
  }

  cancel (cancelTokenSource, message) {
    if (!cancelTokenSource) {
      return
    }

    if (typeof message !== 'string') {
      message = 'Operation canceled by the user.'
    }

    cancelTokenSource.cancel(message)
  }
}
