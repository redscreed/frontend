import store from '@/store'

const authenticated = (to, from, next) => {
  const { 'auth/authenticated': authenticated } = store.getters
  authenticated() ? next() : next({ name: 'Auth' })
}

export default authenticated
