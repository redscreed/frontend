import store from '@/store'

const guest = (to, from, next) => {
  const { 'auth/authenticated': authenticated } = store.getters
  authenticated() ? next({ name: 'Home' }) : next()
}

export default guest
