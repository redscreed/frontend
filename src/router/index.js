import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthPage from '../pages/AuthPage'
import RegisterPage from '../pages/RegisterPage'
import GuestGuard from './guard/guest'
import AuthenticatedGuard from './guard/authenticated'
import HomePage from "../pages/HomePage"
import SalaryEditPage from "../pages/SalaryEditPage"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomePage,
    beforeEnter: AuthenticatedGuard
  },
  {
    path: '/salary/:id/',
    name: 'Salary',
    component: SalaryEditPage,
    beforeEnter: AuthenticatedGuard
  },
  {
    path: '/auth',
    name: 'Auth',
    component: AuthPage,
    beforeEnter: GuestGuard
  },
  {
    path: '/register',
    name: 'Register',
    component: RegisterPage,
    beforeEnter: GuestGuard
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.VUE_APP_BASE_PATH || '/',
  routes
})

export default router
