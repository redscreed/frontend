const checkToken = (token) => {
  if (!token || token.split('.').length < 3) {
    return false
  }

  const data = JSON.parse(atob(token.split('.')[1]))

  return new Date() < new Date(data.exp * 1000)
}

const set = (token) => localStorage.setItem('token', token)

const get = () => {
  const token = localStorage.getItem('token')
  if (!checkToken(token)) return null

  return token
}

const remove = () => localStorage.removeItem('token')

export default {
  get,
  set,
  remove,
  checkToken
}
