import Vue from 'vue'
import Axios from 'axios'

import TokenStorage from '../core/auth/token-storage'

// allow use http client without Vue instance
const http = Axios.create({
  baseURL: process.env.VUE_APP_API_URL || '/api/',
  headers: {
    common: {
      Accept: 'application/json',
    }
  }
})

const token = TokenStorage.get()
if (token) {
  http.defaults.headers.common.Authorization = `Bearer ${token}`
}

http.interceptors.response.use(
  (response) => response,
  (error) => {
    const { response } = error
    if (typeof response !== 'undefined' && response.status === 401) {
      Vue.$router.push({ name: 'auth-signout' })
    }

    return Promise.reject(error)
  }
)

Object.defineProperty(Vue.prototype, '$http', {
  get () {
    return http
  }
})

export default http
