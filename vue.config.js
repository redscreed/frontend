module.exports = {
  devServer: {
    disableHostCheck: true,
    public: process.env.PUBLIC || 'panel.bitofme.local'
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  }
}
